# golang.org/x/crypto/ssh Denial of Service

This repository contains a proof of concept exploit for CVE-2020-29652 it
triggers a nil pointer dereference in `golang.org/x/crypto/ssh` based SSH
servers prior to ` v0.0.0-20201216223049-8b5274cf687f`.


## Details

The server implementation in `golang.org/x/crypto/ssh` used this `switch`
statement (shortened sections indicated by `[...]`)

```go
		switch userAuthReq.Method {
		case "none":
			if config.NoClientAuth {
				authErr = nil
			}

			// allow initial attempt of 'none' without penalty
			if authFailures == 0 {
				authFailures--
			}
		case "password":
			if config.PasswordCallback == nil {
				authErr = errors.New("ssh: password auth not configured")
				break
			}
[...]
		case "gssapi-with-mic":
			gssapiConfig := config.GSSAPIWithMICConfig
			userAuthRequestGSSAPI, err := parseGSSAPIPayload(userAuthReq.Payload)
[...]
			authErr, perms, err = gssExchangeToken(gssapiConfig, userAuthGSSAPITokenReq.Token, s, sessionID,
				userAuthReq)
			if err != nil {
				return nil, err
			}
		default:
			authErr = fmt.Errorf("ssh: unknown method %q", userAuthReq.Method)
		}
```

In the above we can see a call to `gssExchangeToken` which starts as follows:

```go
func gssExchangeToken(gssapiConfig *GSSAPIWithMICConfig, firstToken []byte, s *connection,
	sessionID []byte, userAuthReq userAuthRequestMsg) (authErr error, perms *Permissions, err error) {
	gssAPIServer := gssapiConfig.Server
```

The above `switch` statement lacks a check if `gssapiConfig :=
config.GSSAPIWithMICConfig` would assign `nil` to `gssapiConfig` when
`gssapi-with-mic` based authentication is not configured. So this leads to a
`nil` pointer dereference as soon as we force the server to parse a
`gssapi-with-mic` authentication attempt.

To be able to have a client ignore the server side proposed authentication
mechanisms and just send an arbitrary authentication mechanism of the client's
choice this repository contains a modified version of `golang.org/x/crypto/ssh`
